﻿using System;
using System.Collections.Generic;

namespace AllLongestStrings
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] s = new string[5]{"aba", "aa", "ad", "vcd", "aba"};
            string[] s1 = new string[8]{"a", "abc", "cbd", "zzzzzz", "a",
             "abcdef", "asasa", "aaaaaa"};
             string[] s2 = new string[1] {"aa"};
            
            //string[] newS = new string[3]{ allLongestStrings(s)};
            allLongestStrings(s);
            allLongestStrings(s1);

            allLongestStrings(s2);

             

        }

        public static string[] allLongestStrings(string[] inputArray) {
            List<string> l = new List<string>();
            int maxLength = 0;
            for (int i = 0; i < inputArray.Length; i++)
            {
                if(inputArray[i].Length > maxLength){
                    maxLength = inputArray[i].Length;
                }
            }

            for (int i = 0; i < inputArray.Length; i++)
            {
                if(inputArray[i].Length == maxLength){
                    l.Add(inputArray[i]);
                }
            }

            foreach (var item in l)
            {
                System.Console.WriteLine(item);
            }

            string[] result = l.ToArray();
            return result;
        }

    }
}
